const fs = require('fs');
const {exec} = require('child_process');
const rootProjectUrl = process.cwd();
const packageJSON = require(rootProjectUrl + '/package.json');
const gitDomain = packageJSON.gitDomain;

const gitDependencies = packageJSON.gitDependencies;

for (let package in gitDependencies) {
    let repo = gitDependencies[package];
    let repoPath = repo.split('#')[0];
    let revision = repo.split('#')[1];
    if (!revision) {
        console.error('\x1b[31m%s\x1b[0m', `Please specify revision (tag, branch, commit hash) for package ${package}`);
    } else {
        let repoUrl = `https://${gitDomain}/${repoPath}.git`;
        let destination = `node_modules/${package}`;
        fs.stat(destination, (err, result) => {
            let command;
            if (err && err.errno === 34) {
                // directory not exists, can clone the repo
                command = `git clone --branch ${revision} --single-branch ${repoUrl} ${destination}`;
            } else {
                // directory already exists, remove the directory before cloning the repo
                if (process.platform === 'win32') {
                    command = `rmdir ${destination} & git clone --branch ${revision} --single-branch ${repoUrl} ${destination}`;
                } else {
                    command = `rm -rf ${destination} && git clone --branch ${revision} --single-branch ${repoUrl} ${destination}`
                }
            }

            exec(command, (err, stdout, stderr) => {
                console.log(stdout);
                console.error(stderr);
            });
        })
        
    }
}