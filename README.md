# How to add private Git repository as NPM dependencies

In your `package.json` file:

- Add `postinstall` script:
```json
"scripts": {
    "postinstall": "node node_modules/install-private-git"
}
```

- Add `devDependencies` the tool to install private git repo
```json
"devDependencies": {
    "install-private-git": "git+https://gitlab.vndirect.com.vn/anh.lamminh/install-private-git.git"
}
```

- Configure git repos as NPM dependencies
```json
"gitDomain": "gitlab.vndirect.com.vn",
"gitDependencies": {
    "repo1": "<groupName>/<repoName>#1.2.0",
    "repo2": "<groupName>/<repoName>#master"
}
```
All dependencies will be install into `node_modules` directory

Run `yarn` command to install dependencies

Done!!